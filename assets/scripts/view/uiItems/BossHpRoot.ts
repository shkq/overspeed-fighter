// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class BossHpRoot extends cc.Component {

    @property(cc.ProgressBar)
    bossHpPro: cc.ProgressBar = null;

    maxHP: number = 80;
    // onLoad () {}

    start () {

    }

    initData(bossHP: number){
        this.maxHP = bossHP;
        this.bossHpPro.progress = 1;
    }

    updateBossPro(preHp: number){
        this.bossHpPro.node.stopAllActions();
        let value = preHp/this.maxHP;
        cc.tween(this.bossHpPro)
        .to(0.2, {progress: value})
        .start();
    }

    // update (dt) {}
}
