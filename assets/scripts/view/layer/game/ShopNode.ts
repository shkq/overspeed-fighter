
import Utils from '../../../common/utils/Utils';
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import { skill_data_text } from '../../../dataUtils/DataClass';
import { data_manager } from '../../../dataUtils/DataManeger';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShopNode extends ViewBase {

    @property([cc.Node])
    itemNode: cc.Node[] = [];
    @property([cc.Sprite])
    itemIconSpr: cc.Sprite[] = [];

    @property(cc.Node)
    shareBtn: cc.Node = undefined;

    @property([cc.SpriteFrame])
    itemBgFrame: cc.SpriteFrame[] = [];

    indexs: Array<number> = [];
    levelColors: Array<cc.Color> = [new cc.Color(255, 255, 255), new cc.Color(252, 255, 198), new cc.Color(69, 192, 255), new cc.Color(205, 252, 255), new cc.Color(255, 158, 0)]

    @property(cc.Node)
    huiFuBtn: cc.Node = undefined;

    protected onLoad(): void {
        gameScene.pauseGame();
        this.randomIndex();
        this.initItems();
        pfApi.stopRecordVideo();
    }

    randomIndex() {
        let weights = [];
        for (let i = 0; i < data_manager.skill_data.length; i++) {
            weights.push(data_manager.skill_data[i].weight);
        }

        for (let i = 0; i < 3; i++) {
            let index = Utils.weightsRadom(weights);
            let data = data_manager.skill_data[index];
            if (data.limit_value != 0) {
                if (data.add_value > 0) {
                    if (data.add_key.indexOf("skill") != -1) {
                        if (gameScene.heroCtrl.skill_ctrl.skillMap[data.add_key]) {
                            if (gameScene.heroCtrl.skill_ctrl.skillMap[data.add_key].bullet_count >= data.limit_value) {
                                weights[index] = 0;
                                i--;
                                continue;
                            }
                        }
                    } else {
                        if (gameScene.heroCtrl[data.add_key] >= data.limit_value) {
                            weights[index] = 0;
                            i--;
                            continue;
                        }
                    }
                } else {
                    if (gameScene.heroCtrl[data.add_key] <= data.limit_value) {
                        weights[index] = 0;
                        i--;
                        continue;
                    }
                }
            }
            weights[index] = 0;
            this.indexs.push(index);
        }
        // if(this.indexs.indexOf(10) != -1){
        //     this.huiFuBtn.active = false;
        // }
    }

    initItems() {
        for (let i = 0; i < 3; i++) {
            let data = data_manager.skill_data[this.indexs[i]];
            // this.itemNode[i].getChildByName("icon").getComponent(cc.Sprite).spriteFrame
            this.itemNode[i].getChildByName("nameLabel").getComponent(cc.Label).string = data.name;
            this.itemNode[i].getChildByName("infoLabel").getComponent(cc.Label).string = this.initSkillInfo(data.info, data.add_key, data);
            this.itemNode[i].getChildByName("count").getComponent(cc.Label).string = data.star_count + "";
            this.itemNode[i].getComponent(cc.Sprite).spriteFrame = this.itemBgFrame[data.level - 1];
            this.itemNode[i].getChildByName("infoLabel").color = this.levelColors[data.level - 1];
            this.itemNode[i].getChildByName("nameLabel").color = this.levelColors[data.level - 1];
            this.itemIconSpr[i].spriteFrame = cUI.getTextureSync($.Texture.shopItems + data.add_key);
        }
    }

    onBtnSelect(e: cc.Event, index: string) {
        // cSound.playEffect($.Sound.btn_0);
        let i = Number(index);
        let data = data_manager.skill_data[this.indexs[i]];
        // if (gameData._xing_guang_count < data.star_count) {
        //     cUI.showTips("星光不足");
        //     console.log('--------------', data.star_count, gameData._xing_guang_count);
        //     return;
        // }
        // gameData._xing_guang_count -= data.star_count;
        // this.itemNode[i].getChildByName("tip").getComponent(cc.Label).string = "已兑换";
        // this.itemNode[i].getComponent(cc.Button).interactable = false;
        // this.itemNode[i].color = new cc.Color(100, 100, 100);
        // this.itemNode[i].opacity = 180;
        if (data.add_key.indexOf("skill") != -1) {
            gameScene.heroCtrl.updateSkill(data.add_key);
        } else {
            this.changePlayerData(data);
        }
        this.onBtnExit();

        // gameScene.GameUI.updateStar();

        // this.node.getChildByName("_btn_shuaxin").getComponent(cc.Button).interactable = false;
        // this.node.getChildByName("_btn_shuaxin").opacity = 150;
    }

    onBtnShuaXin(){
        cSound.playEffect($.Sound.btn_0);

        pfApi.showVideoAd(this.videoShuaXin.bind(this));
    }

    videoShuaXin(){
        this.indexs = [];
        this.randomIndex();
        this.initItems();
        pfApi.reportAnalytics('shuaXin', {});

    }

    onHuiFuBtn(){
        cSound.playEffect($.Sound.btn_0);

        pfApi.showVideoAd(this.videoHuiFu.bind(this));
    }

    videoHuiFu(){
        let data = data_manager.skill_data[10];
        this.changePlayerData(data);
        pfApi.reportAnalytics('huiFu', {});

    }

    changePlayerData(data: skill_data_text) {
        gameScene.heroCtrl.changeData(data);
    }

    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        gameScene.resumeGame();
        gameScene.EnemyRoot.updateEnemyData();
        gameScene.EnemyRoot.addEnemy();
        pfApi.startRecordVideo(150);
    }

    initSkillInfo(info: string, key: string, data: skill_data_text) {
        let index = info.indexOf("N");
        let count = 1;
        if (index != -1) {
            let skillMap = gameScene.heroCtrl.skill_ctrl.skillMap;
            if(data.add_key.indexOf("_zd") != -1){
                skillMap = gameScene.heroCtrl.skill_ctrl.skillZdMap;
            }
            if (skillMap[key]) {
                count = skillMap[key].bullet_count + 1;
            }
            if(data.add_key == "skill_dianci_zd"){
                count = (2700 + count*data.add_value)/ 1000;
            }
            info = info.replace("N", count.toString());
        }
        return info;
    }

    onBtnShare(){
        cSound.playEffect($.Sound.btn_0);
        pfApi.onShareAppVideo(this.ShareFinish.bind(this));
    }

    ShareFinish(){
        this.shareBtn.active = false;
    }

}