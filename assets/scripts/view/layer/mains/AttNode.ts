
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import { p_data_mager } from '../../../Mangers/playerDataMager/PlayerDataMager';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const { ccclass, property } = cc._decorator;

@ccclass
export default class AttNode extends ViewBase {

    @property(cc.Node)
    showAttItem: cc.Node = undefined;
    @property(cc.Label)
    preLb: cc.Label = undefined;
    @property(cc.Label)
    nextLb: cc.Label = undefined;
    @property(cc.Node)
    attItemRoot: cc.Node = undefined;

    @property(cc.Node)
    selectFlag: cc.Node = undefined;

    _index: number = 0;

    protected onLoad(): void {
        for (let i = 0; i < 8; i++) {
            this.updateAttItem(i);
        }
        this.updateShowAttNode();
    }


    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
    }

    onBtnSelectAtt(e: cc.Event.EventTouch, data: string) {
        cSound.playEffect($.Sound.btn_0);
        this._index = Number(data);
        this.updateShowAttNode();
    }

    onBtnUpgrade(e: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        let maxLv = 3;
        if (this._index == 7) {
            maxLv = 1;
        }
        if (p_data_mager.playerData.attLv[this._index] >= maxLv) {//已经满级了
            cUI.showTips("该属性满级了");
        } else {
            pfApi.showVideoAd(this.videoFinish.bind(this));
        }
    }

    videoFinish() {
        cUI.showTips("升级成功");
        p_data_mager.playerData.attLv[this._index]++;
        this.updateShowAttNode();
        this.updateAttItem(this._index);
        p_data_mager.saveData();
        pfApi.reportAnalytics('upgradeAtt', {
            type: p_data_mager.attArr[this._index],
          });
    }

    /**
     * 更新右边对应属性节点的显示
     * @param index 
     */
    updateAttItem(index: number) {
        let item = this.attItemRoot.getChildByName("attItem_" + index);
        for (let i = 0; i < p_data_mager.playerData.attLv[index]; i++) {
            item.getChildByName("lvItem_" + i).getChildByName("lvFlag").active = true;
        }
    }

    /**更新左边展示框的属性信息 */
    updateShowAttNode() {
        let item = this.attItemRoot.getChildByName("attItem_" + this._index);
        this.showAttItem.getChildByName("icon").getComponent(cc.Sprite).spriteFrame = item.getChildByName("icon").getComponent(cc.Sprite).spriteFrame;
        this.showAttItem.getChildByName("nameLb").getComponent(cc.Label).string = item.getChildByName("nameLb").getComponent(cc.Label).string;
        this.selectFlag.parent = item;

        let maxLv = 3;
        if (this._index == 7) {
            maxLv = 1;
        }
        let value = p_data_mager.getAddAttValue(this._index);
        this.preLb.string = "当前: " + value
        if (p_data_mager.playerData.attLv[this._index] >= maxLv) {//已经满级了
            this.showAttItem.parent.getChildByName("jiantou").active = false;
            this.nextLb.string = "已满级";
        } else {
            this.showAttItem.parent.getChildByName("jiantou").active = true;
            value = p_data_mager.getAddAttValue(this._index,1);
            this.nextLb.string = "下一级：" + value;
        }

    }

}