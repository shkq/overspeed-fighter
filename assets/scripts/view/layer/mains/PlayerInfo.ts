
import { HttpRequest } from '../../../common/utils/HttpRequest';
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import { p_data_mager } from '../../../Mangers/playerDataMager/PlayerDataMager';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const { ccclass, property } = cc._decorator;

@ccclass
export default class PlayerInfo extends ViewBase {

    @property(cc.Sprite)
    avatarSpr: cc.Sprite = undefined;
    @property(cc.Sprite)
    rankIcon: cc.Sprite = undefined;
    @property(cc.Label)
    nameLb: cc.Label = undefined;
    @property(cc.Label)
    rankLb: cc.Label = undefined;
    @property(cc.Label)
    killCountLb: cc.Label = undefined;
    @property(cc.Label)
    maxLifeLb: cc.Label = undefined;
    @property(cc.Label)
    zhanLiLb: cc.Label = undefined;

    protected onLoad(): void {
        HttpRequest.updateHttpSprite(p_data_mager.playerData.avatarUrl, this.avatarSpr);
        this.initView();
        console.log('------THIS.node:', this.node);
    }


    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
    }

    onBtnAttNode(e: cc.Event.EventTouch, data: string) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        cUI.addLayer($.Prefab.attNode);
    }

    initView(){
        this.nameLb.string = "昵称："+p_data_mager.playerData.nickName;
        this.rankLb.string = "称号：" + p_data_mager.playerData.rank;
        this.killCountLb.string = "累计歼敌：" + p_data_mager.playerData.killRivalCount;
        this.maxLifeLb.string = "最大周期 ：" + p_data_mager.playerData.maxLifeDay;
        this.zhanLiLb.string = "战力："+p_data_mager.getZhanLi();
        cUI.getTexture($.Texture.rankIcon + p_data_mager.getRankIndex()).then(value =>{
            this.rankIcon.spriteFrame = value;
        })
    }

}