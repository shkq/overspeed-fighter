// 2.x 
cc.Component.prototype.addAutoReleaseAsset = function (_asset: cc.Asset) {
    let oneTempAuto = this.node.getComponent(AutoReleaseAssets);
    if (!cc.isValid(oneTempAuto)) {
        oneTempAuto = this.node.addComponent(AutoReleaseAssets);
    };
    oneTempAuto.addAutoReleaseAsset(_asset);
};
cc.Component.prototype.addAutoReleaseAssets = function (_assets: cc.Asset[]) {
    let moreTempAuto = this.node.getComponent(AutoReleaseAssets);
    if (!cc.isValid(moreTempAuto)) {
        moreTempAuto = this.node.addComponent(AutoReleaseAssets);
    };
    for (const _assetSelf of _assets) {
        moreTempAuto.addAutoReleaseAsset(_assetSelf);
    };
};



// AutoReleaseAssets
const { ccclass, menu, disallowMultiple } = cc._decorator;
@ccclass
@menu('资源管理/AutoReleaseAssets/自动释放资源')
@disallowMultiple
export default class AutoReleaseAssets extends cc.Component {

    private dynamicsAssets: cc.Asset[] = [];

    // onLoad () {}
    public addAutoReleaseAsset(_asset: cc.Asset) {
        if (cc.isValid(_asset)) {
            _asset.addRef();
            this.dynamicsAssets.push(_asset);
            console.log("当前动态资源长度=="+this.dynamicsAssets.length);
        }
    };
    onDestroy(): void {
        // console.log("继承cc.Component拥有生命周期如果Node销毁就会顺带销毁这里"); 
        for (let index = 0; index < this.dynamicsAssets.length; index++) {
            if (cc.isValid(this.dynamicsAssets[index])) {
                this.dynamicsAssets[index].decRef();
            }
        }
        this.dynamicsAssets = [];
    };
};