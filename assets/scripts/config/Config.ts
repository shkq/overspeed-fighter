import { Atlas, Prefab, Sound, Texture, TextDataRes } from './Res';
import LocalStorage from '../common/module/LocalStorage';
import UI from '../common/module/UI';
import EventEmitter from '../common/module/EventEmitter';
import Pool from '../common/module/Pool';
import GameSound from '../common/module/Sound';
import PlatformApi from '../common/module/PlatformApi';
import GameStateData from '../ctrls/GameStateData';
/*全局通用管理模块*/
export var cSound = GameSound.get();
export var cStorage = LocalStorage.get();
export var cUI = UI.get();
export var cEvent = EventEmitter.get();
export var cNodePool = Pool.get();
export var pfApi = PlatformApi.get();
export var gameData = GameStateData.get();
/*常量集合*/
export var $ = {
    Sound, Prefab, Texture, Atlas,TextDataRes
};