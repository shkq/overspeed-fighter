
export var Prefab = {
	loading: 'prefab/component/loading',
	mask: 'prefab/component/mask',
	net_loading: 'prefab/component/net_loading',
	tips_item: 'prefab/component/tips_item',

	notice: 'prefab/layer/common/notice',
	pause: 'prefab/layer/common/pause',

	gameResult: 'prefab/layer/game/gameResult',
	rerivalNode: 'prefab/layer/game/rerivalNode',
	shopNode: 'prefab/layer/game/shopNode',

	attNode: 'prefab/layer/mains/attNode',
	playerInfo: 'prefab/layer/mains/playerInfo',
	strongStart: 'prefab/layer/mains/strongStart',

	game_scene: 'prefab/scene/game_scene',
	main_scene: 'prefab/scene/main_scene',

	enemyBulletBoom: "prefab/items/pars/enemyBulletBoom",
	boss: "prefab/items/pars/boss",
	fangkuai_huang: "prefab/items/pars/fangkuai_huang",
	yuan_lv: "prefab/items/pars/yuan_lv",
	enemyDeathPars: "prefab/items/pars/",
	showEnemy: "prefab/items/pars/showEnemy",
	
	yuan_bullet: "prefab/items/yuan_bullet",
	enemy_bullet: "prefab/items/enemy_bullet",
	enemy_fangkuai_huang:  "prefab/items/enemy_fangkuai_huang",
	enemy:  "prefab/items/",
	xingxing:  "prefab/items/xingxing",

	skills:  "prefab/items/skills/",

	showBoss: "prefab/items/pars/showBoss",
	boss_0: "prefab/items/boss_0",
	boss_1: "prefab/items/boss_1",
	boss_2: "prefab/items/boss_2",
	boss_3: "prefab/items/boss_3",
	boss_4: "prefab/items/boss_4",
	boss_5: "prefab/items/boss_5",

};
export var Atlas = {

};
export var Sound = {
	game_bgm: 'sound/bgm/game_bgm',
	mainBgm: 'sound/bgm/mainBgm',
	bossBgm: 'sound/bgm/bossBgm',


	btn_0: 'sound/btn/btn_0',
	fail: 'sound/effect/fail',
	qifei: 'sound/other/qifei',

	jizhong0: 'sound/effect/jizhong0',
	jizhong1: 'sound/effect/jizhong1',
	jizhong2: 'sound/effect/jizhong2',
	jizhong3: 'sound/effect/jizhong3',
	showEnemy0: 'sound/effect/showEnemy0',
	showEnemy1: 'sound/effect/showEnemy1',
	shoot0: 'sound/effect/shoot0',
	pickup: 'sound/effect/pickup',
	shandian0: 'sound/effect/shandian0',
	shandian1: 'sound/effect/shandian1',
	shandian2: 'sound/effect/shandian2',

	boss_death: 'sound/effect/boss_death',
	boss_hit: 'sound/effect/boss_hit',

};
export var Texture = {
	shopItems: 'texture/shopItems/',
	rankIcon: 'texture/rankIcon/',
	// sdlr2: 'texture/skill_icon/sdlr2',
};

export var TextDataRes = {
	skill_data: 'textData/skill_data',
}