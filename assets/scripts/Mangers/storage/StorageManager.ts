import { md5 } from "../../libs/security/Md5";
import { EncryptUtil } from "./EncryptUtil";

export module Storage {
    let _key: string = "yinglingduijue";
    let _iv: string = "yinglingduijue";
    let _id: number = 102500;
    let debug: boolean = false;

    /**
     * 初始化密钥
     * @param key aes加密的key
     * @param iv aes加密的iv
     */
    export function init(key: string, iv: string) {
        _key = md5(key);
        _iv = md5(iv)
    }

    /**
     * 设置用户id
     * @param id 
     */
    export function serUser(id: number) {
        _id = id;
    }

    /**
     * 存储
     * @param key 存储key
     * @param value 存储值
     */
    export function set(key: string, value: any) {
        key = `${key}_${_id}`;
        if (null == key) {
            console.error("存储的key不能为空");
            return;
        }
        if (!CC_PREVIEW || !debug) {
            key = md5(key)
        }
        if (value == null) {
            console.warn("存储的值为空，则直接移除该存储");
            remove(key);
            return;
        }
        if (typeof value == "function") {
            console.error("存储的值不能为方法")
            return;
        }
        if (typeof value === "object") {
            try {
                value = JSON.stringify(value);
            } catch (error) {
                console.error(`解析失败， str = ${value}`);
                return;
            }
        } else if (typeof value === "number") {
            value = value + "";
        }

        if ((!CC_PREVIEW || !debug) && null != _key && null != _iv) {
            try {
                value = EncryptUtil.aesEncrypt(value, _key, _iv);
            } catch (error) {
                value = null;
            }
        }
        // console.log('--------------key:',key, value);
        cc.sys.localStorage.setItem(key, value);
    }

    /**
     * 获取存储数据
     * @param key 
     * @param defaultValue 默认初始化数据
     */
    export function get(key: string, defaultValue?: any) {
        if (null == key) {
            console.error("存储的key不能为空");
            return;
        }
        key = `${key}_${_id}`;
        if (!CC_PREVIEW || !debug) {
            key = md5(key)
        }
        let str: string = cc.sys.localStorage.getItem(key);
        // console.log('------111--------get:',key, str,_key,_iv);
        if (null != str && "" != str && (!CC_PREVIEW || !debug) && null != _key && null != _iv) {
            try {
                str = EncryptUtil.aesDecrypt(str, _key, _iv);
            } catch (error) {
                console.error('------111--------error:', error);

                str = null;
            }
        }
        if (null == defaultValue || typeof defaultValue === 'string') {
            return str;
        }
        if (null === str) {
            return defaultValue;
        }
        if (typeof defaultValue === 'number') {
            return Number(str) || 0;
        }
        if (typeof defaultValue === 'boolean') {
            return "true" == str; // 不要使用Boolean("false");
        }
        if (typeof defaultValue === 'object') {
            try {
                return JSON.parse(str);
            }
            catch (e) {
                console.error("解析数据失败,str=" + str);
                return defaultValue;
            }

        }
        // console.log('--------------get:',key, str);
        return str;
    }

    /**
     * 移除存储值
     * @param key 
     */
    export function remove(key: string) {
        if (null == key) {
            console.error("存储的key不能为空");
            return;
        }
        key = `${key}_${_id}`;
        if (!CC_PREVIEW || !debug) {
            key = md5(key);
        }
        cc.sys.localStorage.removeItem(key);
    }
}