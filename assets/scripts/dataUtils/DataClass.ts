/**
 * 升级属性的数值数据
 */
export class skill_data_text {
    //id
    id: number = 0;
    //名称
    name: string = "";
    //说明
    info: string = "";
    //属性标识
    add_key: string = "";
    //增幅的数值
    add_value: number = 0;
    //增加的数值是不是百分比
    add_percent: boolean = false;
    //减少属性标识
    sub_key: string = "";
    //减少的数值
    sub_value: number = 0;
    //兑换星光数
    star_count: number = 5;
    //出现权重
    weight: number = 100;
    //等级
    level: number = 1;
    //最大最小限制
    limit_value: number = 0;
    //技能冷却时间
    time_cd: number = 5000;
    //技能项的属性增加标识
    skill_add_key: string = '';
    //技能效果持续时间
    duration_time: number = 1;
    //弹道间隔
    time_bullet: number = 150;
    //弹道数量
    bullet_count: number = 1;
    //减少的数值是不是百分比
    sub_percent: boolean = false;
}