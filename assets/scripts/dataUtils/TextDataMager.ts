import { $, cUI } from "../config/Config";
import { skill_data_text } from "./DataClass";
import { data_manager } from "./DataManeger";

const { ccclass, property } = cc._decorator;
export var p_TextDataMager: TextDataMager = undefined;
@ccclass
export default class TextDataMager extends cc.Component {

    onLoad() {
        p_TextDataMager = this;
        for (let key in $.TextDataRes) {
            // console.log('---------------key: ', key, $.TextDataRes[key]);
            this.loadTextData(key);
        }
    }

    loadTextData(urlKey: string) {
        cUI.geTextAsset($.TextDataRes[urlKey]).then(data => {
            // p_DataMager[urlKey]
            let strArr = this.format_data_text(data.text);
            this['get_' + urlKey](strArr, data_manager[urlKey]);
            // if(urlKey == "up_attributes_data"){
            //     this.get_up_attributes_data(strArr, data_manager[urlKey]);
            // }else if( urlKey == "hero_attributes_data"){

            // }
            // console.log('----------set date after:', urlKey, " :", data_manager[urlKey]);
        })
    }

    //格式化配置表数据
    format_data_text(text: string) {
        text = text.replace(/\r/g, "");
        let str1 = text.split('\n');
        return str1;
    }

    /**
    * 获取属性升级的配置表
    */
    get_skill_data(strArr: string[], set_pos: skill_data_text[]) {
        for (let i = 2; i < strArr.length; i++) {
            let str2 = strArr[i].split(',');
            let data = new skill_data_text();
            data.id = Number(str2[0]);
            data.name = str2[1];
            data.add_key = str2[2];
            data.add_value = Number(str2[3]);
            data.add_percent = Number(str2[4]) == 1 ? true : false;
            data.sub_key = str2[5];
            data.sub_value = Number(str2[6]);
            data.sub_percent = Number(str2[7]) == 1 ? true : false;
            data.star_count = Number(str2[8]);
            data.weight = Number(str2[9]);
            data.level = Number(str2[10]);
            data.limit_value = Number(str2[11]);
            data.time_cd = Number(str2[12]);
            data.skill_add_key = str2[13];
            data.time_bullet = Number(str2[14]);
            data.duration_time = Number(str2[15]);
            data.bullet_count = Number(str2[16]);
            data.info = str2[17];

            set_pos.push(data);
        }
    }

}
