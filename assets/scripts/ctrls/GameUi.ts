// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../common/utils/Utils";
import { gameData } from "../config/Config";
import zd_skill_ctrl from "../items/skills/zd_skill_ctrl";
import { gameScene } from "../view/scene/GameScene";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameUi extends cc.Component {

    @property(cc.Label)
    hpLabel: cc.Label = null;
    @property(cc.ProgressBar)
    hpPro: cc.ProgressBar = null;

    @property(cc.Label)
    starLabel: cc.Label = null;

    @property(cc.Label)
    waveLabel: cc.Label = null;

    @property(cc.Label)
    waveTimeLabel: cc.Label = null;

    @property(cc.Node)
    skillNode: cc.Node = null;
    @property(cc.Prefab)
    zd_skill_ctrl: cc.Prefab = null;

    @property(cc.Label)
    enemyCount: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.updateStar();
        this.updateWave();
        this.updateTimeLabel();
        this.updateEnemy();
    }

    updateStar(){
        this.starLabel.string = gameData._xing_guang_count + "";
    }

    updateWave(){
        this.waveLabel.string = (gameData._enemy_wave+1) + "";
    }

    updateEnemy(){
        this.enemyCount.string = (gameScene.EnemyRoot.node.childrenCount) + "";
    }

    updateTimeLabel(){
        let time = Utils.formatMinSeconds(gameData._kill_enemy_time/1000);
        this.waveTimeLabel.string = time;
    }

    addZdSkill(key: string){
        let node = cc.instantiate(this.zd_skill_ctrl);
        node.name = key;
        node.parent = this.skillNode;
        node.getComponent(zd_skill_ctrl).initData(key);
    }

    updateZdSkill(key: string){
        let node = this.skillNode.getChildByName(key);
        node.getComponent(zd_skill_ctrl).initData(key);
    }

    // update (dt) {}
}
