
const { ccclass, property } = cc._decorator;

@ccclass
export default class ShaderTime extends cc.Component {
    _material: cc.Material = null;

    @property
    _max: number = 1;

    addFlag: number = 0.002;

    isUpdate: boolean;

    @property
    get max(): number {
        return this._max;
    }
    set max(value) {
        this._max = value;
        // if (!CC_EDITOR) {
        //     return;
        // }

        // let sprite = this.node.getComponent(cc.Sprite);
        // if (sprite) {
        //     this._material = this.getComponent(cc.Sprite).getMaterial(0);
        //     // if (this._material._properties.time) {
        //     let material: any = sprite.getMaterial(0);;
        //     material.setProperty('timeTest', value);
        //     // }
        // }
    }

    private _start = 0;

    protected update(dt) {
        this._material = this.node.getComponent(cc.Sprite).getMaterial(0);
        // console.log('  this._material',  this._material);

        // if (this.node.active && this._material && this._material.effect._properties.timeTest) {
        if (this.node.active && this._material) {
            // console.log(11111111111);

            this._setShaderTime(dt);
        }
    }
    private _setShaderTime(dt) {
        if(this._start > this._max){
            this.addFlag = -0.002;
        }else if(this._start < 0){
            this.addFlag = 0.002;
        }
        this._start +=  this.addFlag;
        this._material.setProperty('timeTest', this._start);

    }
}
