import Utils from "../../common/utils/Utils";
import { $, cNodePool } from "../../config/Config";
import { gameScene } from "../../view/scene/GameScene";

export var SkillList = {
    /**御雷诀 */
    skill_shenlei(key: string, time_bullet: number, bullet_count: number, td_Node?: cc.Node) {
        let time = time_bullet / 1000;
        let str = $.Prefab.skills + key;
        gameScene.schedule(() => {
            if (gameScene.is_pause) return;
            console.log('---------------str:', str);
            cNodePool.get(str).then(value => {
                value.parent = gameScene.skillRootNode;
                let enemyPos;
                if(gameScene.EnemyRoot.node.childrenCount > 0){
                    // let index = Utils.getNumMinToMax(0,gameScene.EnemyRoot.node.childrenCount-1);
                    enemyPos =gameScene.EnemyRoot.getCloserEnemyPos(gameScene.heroCtrl.getPos());
                }else{
                    enemyPos = Utils.getRadomPos(gameScene.heroCtrl.getPos(), 2.3, true);
                }
                value.getComponent(key).startSkill(enemyPos, key);
                value.getComponent(key).startAnim();
            })
        }, time, bullet_count - 1);
    },

    /**电磁静默 */
    skill_dianci_zd(key: string, time_bullet: number, bullet_count: number,duration_time: number, td_Node?: cc.Node){
        console.log('---------------释放；电磁静默')
        let str = $.Prefab.skills + key;
        cNodePool.get(str).then(value => {
            value.parent = gameScene.PropRoot.node;
            value.zIndex = -1;
            let enemyPos = gameScene.heroCtrl.getPos();
            value.getComponent(key).startSkill(enemyPos, key);
            value.getComponent(key).startAnim();
        })
    }
}