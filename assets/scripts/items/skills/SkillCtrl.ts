// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { gameData } from "../../config/Config";
import { data_manager } from "../../dataUtils/DataManeger";
import { gameScene } from "../../view/scene/GameScene";
import { SkillList } from "./SkillList";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SkillCtrl extends cc.Component {

    skillMap: any = {};
    skillZdMap: any = {};

    start() {

    }

    /**
    * 新获取技能或者技能升级时更新角色的技能定时器
    * @param skill_index 当前角色的第几个技能 - 对应第几个定时器
    * @param skill_id 技能id
    */
    updateSkillTimerById(key: string) {
        let skillMap = this.skillMap;
        if (key.indexOf("_zd") != -1) {
            skillMap = this.skillZdMap;
        }

        let skillData = data_manager.getSkillDataByKey(key);

        //已经存在这个技能的数据 - 做更新操作：更新冷却时间，等级
        let count = 1;
        if (!skillMap[key]) {//还没有这个技能的数据-添加
            skillMap[key] = { time_cd: skillData.time_cd, time_bullet: skillData.time_bullet, bullet_count: skillData.bullet_count, duration_time: skillData.duration_time };
            if (key.indexOf("_zd") != -1){
                gameScene.GameUI.addZdSkill(key);
            }
        } else {
            count = skillMap[key].bullet_count + 1;
            skillMap[skillData.skill_add_key] += skillData.add_value;
            if (key.indexOf("_zd") != -1){
                gameScene.GameUI.updateZdSkill(key);
            }
        }
        // skillMap[key].time_bullet = 150;//弹道间隔
        // skillMap[key].duration_time = 150;//技能效果持续时间
        // skillMap[key].time_cd = 3000;//冷却时间
        // skillMap[key].bullet_count = count;//弹道数量
    }


    /**
     * 删除对应的技能定时器
     * @param key 
     */
    deleteSkillTimerByKey(key: string) {
        delete this.skillMap[key];
    }

    //定时器刷新  判断是否需要释放技能
    judgeSkillList() {
        // console.log('--------------judgeSkillList:');
        // return;
        if (gameScene.is_pause) return;
        for (let key in this.skillMap) {
            if (gameData._game_milliscoend % this.skillMap[key].time_cd <= 99) {
                let data = this.skillMap[key];
                SkillList[key](key, data.time_bullet, data.bullet_count);
            }
        }
    }

    // update (dt) {}
}
