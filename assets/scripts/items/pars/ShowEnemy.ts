import Utils from "../../common/utils/Utils";
import { $, cNodePool, cSound } from "../../config/Config";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ShowEnemy extends cc.Component {

    @property(cc.ParticleSystem)
    particle: cc.ParticleSystem = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    colorKey: any = {"yuan_lv": 0,"sanjiaoxing_fen": 1, "fangkuai_huang": 2, "wubianxing_zi": 3, "liubianxing_lan": 4, "sijiaoxing_hong": 5};
    colorArr: cc.Color[] = [new cc.Color(0,255,0),new cc.Color(255,175,173),new cc.Color(255,102,51),new cc.Color(230,0,255),new cc.Color(0,0,255),new cc.Color(255,0,0)];
    initState(key: string, pos: cc.Vec2){
        this.node.position = pos;
        this.particle.startColor = this.colorArr[this.colorKey[key]];
        this.particle.endColor = this.colorArr[this.colorKey[key]];
        this.particle.resetSystem();
        this.scheduleOnce(()=>{
            cNodePool.put(this.node);
        }, 0.8)

        
        let index = Utils.getNumMinToMax(0,1);
        cSound.playEffect($.Sound["showEnemy"+index]);
    }

    stopPar(){
        this.particle.stopSystem();
    }

    clearPar(){
        this.particle.resetSystem();
        this.particle.stopSystem();
    }

    resetPar(){
        this.particle.resetSystem();
    }

    // update (dt) {}
}
