const { ccclass, property } = cc._decorator;

@ccclass
export default class Flash extends cc.Component {

    @property()
    duration: number = 0.5;

    _median: number = 0;
    _time: number = 0;

    _material: cc.Material = null;

    onLoad() {
        this._median = this.duration / 2;
        //获取材质
        if (this.node.getComponent(cc.Sprite)) {
            this._material = this.node.getComponent(cc.Sprite).getMaterial(0);
        } else {
            this._material = this.node.getComponent(sp.Skeleton).getMaterial(0);
        }
        //设置材质对应的属性
        this._material.setProperty("u_rate", 1);
    }

    update(dt) {
        if (this._time > 0) {
            this._time -= dt;
            this._time = this._time < 0 ? 0 : this._time;
            let rate = Math.abs(this._time - this._median) * 2 / this.duration;
            this._material.setProperty("u_rate", rate);
        }
    }

    init() {
        this._time = this.duration;
    }

}