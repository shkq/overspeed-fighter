declare namespace cc {
    interface Tween {
        /*平方曲线缓入函数。*/
        quadIn: string;
        /*平方曲线缓出函数。*/
        quadOut: string;
        /*平方曲线缓入缓出函数。*/
        quadInOut: string;
        /*立方曲线缓入函数。*/
        cubicIn: string;
        /*立方曲线缓出函数。*/
        cubicOut: string;
        /*立方曲线缓入缓出函数。*/
        cubicInOut: string;
        /*四次方曲线缓入函数。*/
        quartIn: string;
        /*四次方曲线缓出函数。*/
        quartOut: string;
        /*四次方曲线缓入缓出函数。*/
        quartInOut: string;
        /*五次方曲线缓入函数。*/
        quintIn: string;
        /*五次方曲线缓出函数。*/
        quintOut: string;
        /*五次方曲线缓入缓出函数。*/
        quintInOut: string;
        /*正弦曲线缓入函数。*/
        sineIn: string;
        /*正弦曲线缓出函数。*/
        sineOut: string;
        /*正弦曲线缓入缓出函数。*/
        sineInOut: string;
        /*指数曲线缓入函数。*/
        expoIn: string;
        /*指数曲线缓出函数。*/
        expoOut: string;
        /*指数曲线缓入和缓出函数。*/
        expoInOut: string;
        /*循环公式缓入函数。*/
        circIn: string;
        /*循环公式缓出函数。*/
        circOut: string;
        /*指数曲线缓入缓出函数。*/
        circInOut: string;
        /*弹簧回震效果的缓入函数。*/
        elasticIn: string;
        /*弹簧回震效果的缓出函数。*/
        elasticOut: string;
        /*弹簧回震效果的缓入缓出函数。*/
        elasticInOut: string;
        /*回退效果的缓入函数。*/
        backIn: string;
        /*回退效果的缓出函数。*/
        backOut: string;
        /*回退效果的缓入缓出函数。*/
        backInOut: string;
        /*弹跳效果的缓入函数。*/
        bounceIn: string;
        /*弹跳效果的缓出函数。*/
        bounceOut: string;
        /*弹跳效果的缓入缓出函数。*/
        bounceInOut: string;
        /*平滑效果函数。*/
        smooth: string;
        /*渐褪效果函数。*/
        fade: string;
    }

    interface Animation {
        fadeIn(name: string, fadeInTime: number, playTimes: number);

        reset();

        gotoAndStopByFrame(name: string, frame: number);

        getState(name: string): { isPlaying: boolean };

        readonly animationNames: Array<string>;
    }

    interface Node {

        click(callback?: Function);

        destroy(): boolean;

        findComponent<T>(type: T): T;

        $<T extends Component>(type: { prototype: T }, inChildrenSearch?: boolean): T;

        $(name: string): Node;

        destroyAllChildrens<T extends Component>(type: { prototype: T });

        show(): void;

        hide(): void;

        toggle(): void;

        hideChildren(): void;

        setLabel(text: string | number): void;

        setText(text: string | number): void;

        setSpriteFrame(spriteFrame: SpriteFrame): void;

        setProgress(percent: number);

        url_name: string;
        is_tween: boolean;
        isChecked: boolean;
        index: number;

        check(): void;
        getPhyCollider(): cc.PhysicsBoxCollider | cc.PhysicsCircleCollider | cc.PhysicsPolygonCollider;
        pos: cc.Vec2;

        uncheck(): void;

        on<T extends Function>(type: 'touchstart', callback: T, target?: any, useCapture?: boolean): T;

        on<T extends Function>(type: 'touchmove', callback: T, target?: any, useCapture?: boolean): T;

        on<T extends Function>(type: 'touchend', callback: T, target?: any, useCapture?: boolean): T;

        on<T extends Function>(type: 'touchcancel', callback: T, target?: any, useCapture?: boolean): T;

        on<T extends Function>(type: 'page-turning', callback: T, target?: any, useCapture?: boolean): T;
    }

    interface Component {
        /*碰撞系统*/

        //当碰撞产生的时候调用
        onCollisionEnter(other, self);

        //当碰撞产生后，碰撞结束前的情况下，每次计算碰撞结果后调用
        onCollisionStay(other, self);

        //当碰撞结束后调用
        onCollisionExit(other, self);

        /*物理系统*/

        // 只在两个碰撞体开始接触时被调用一次
        onBeginContact(contact: PhysicsContact, self: cc.PhysicsPolygonCollider, other: cc.PhysicsPolygonCollider);

        // 只在两个碰撞体结束接触时被调用一次
        onEndContact(contact: PhysicsContact, self: cc.PhysicsPolygonCollider, other: cc.PhysicsPolygonCollider);

        // 每次将要处理碰撞体接触逻辑时被调用
        onPreSolve(contact, self, other);

        // 每次处理完碰撞体接触逻辑时被调用
        onPostSolve(contact, self, other);
    }

    export class Buffer extends Uint8Array {
        length: number;

        write(string: string, offset?: number, length?: number, encoding?: string): number;

        toString(encoding?: string, start?: number, end?: number): string;

        toJSON(): { type: 'Buffer', data: any[] };
    }
}

interface Array<T> {
    /*末位值*/
    readonly last: T;
    init(length: number, value?: number): number[];
}

interface String {
    /*转数字*/
    toNumber(): number;

    /*格式尾部数字*/
    toFormat(num: number | string): string;
    format(num: number | string): string;
}

declare var exports: object;
declare var tt: any;
declare var ks: any;
declare var wx: any;
interface Window {
    tt,
    ks,
    wx
}